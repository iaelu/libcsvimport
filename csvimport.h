#ifndef __CSVIMPORT_H__
#define __CSVIMPORT_H__

#include <stdbool.h>
#include <sys/types.h>

typedef struct _csvctx_t csvctx_t;
typedef struct _linectx_t linectx_t;

struct _csvctx_t {
    const char *filename;
    off_t line_offset;

    char **fields;
    size_t fieldscount;
};

struct _linectx_t {
	const char *filename;
	off_t line_offset;

	char *csvline;
	size_t fieldscount;
};

/**
 * CSV record (line) callback function
 * returns:
 * - 0
 *   data was read successfully, csvimport can deallocate fields data
 * - > 0
 *   data was read successfully, and is still in use. Important: user has to deallocate memory properly.
 * - < 0
 *   data was not successfully read, something is wrong and the import process has to be stopped
 *
 */
typedef int (*csvimport_record_func)(csvctx_t *ctx, void *userdata);

int csvimport(const char *filename, const char sep, csvimport_record_func recordcb, void *userdata);
int csvimport_buffer(const char *buffer, const char sep, csvimport_record_func recordcb, void *userdata);


typedef int (*linesimport_record_func)(linectx_t *ctx, void *userdata);

int linesimport(const char *filename, const char sep, linesimport_record_func recordcb, void *userdata);


#endif /* __CSVIMPORT_H__ */
