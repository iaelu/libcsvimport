#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>

#include "csvimport.h"

//#define VERBOSE

typedef struct _csvimport_ctx_t csvimport_ctx_t;

struct _csvimport_ctx_t {
    char **fields;
    size_t *fieldssz;
    size_t fieldscount;

    char *prevbuf;
    size_t prevbufsz;

    bool escaped;

    csvctx_t *csvctx;
    ssize_t rowcount;
};

static int csvimport_endfield(csvimport_ctx_t *importctx, char *fieldstart, size_t fieldsz);
static int csvimport_endrecord(csvimport_ctx_t *importctx, csvimport_record_func recordcb, void *userdata);
static int csvimport_csvcontinues(csvimport_ctx_t *importctx, char *fieldstart, size_t fieldsz);
static int csvimport_managefield(csvimport_ctx_t *importctx, size_t fieldsz, char **curfield, size_t *cursz);
static void csvimport_ctx_free(csvimport_ctx_t *importctx);
static size_t csvimport_unescape(char *dst);
static void fields_free(char **fields, size_t fieldscount);

#ifndef LINE_MAX
#define LINE_MAX 2048U
#endif
#define CSVLINESZ LINE_MAX


/*
 * desc: csvimport takes a file and a separator character, return every record to recordcb callback function.
 *  User may pass along a pointer to some data he could use inside the callback.
 *  Every record is allocated into memory.
 *  User may deallocate the record array (fields) inside the callback and return true, instead of false.
 *  If false, the csvimport function will deallocate the record array (fields) right after the callback.
 *  The csvimport function has been coded and relies on the RFC 4180, only CRLF(\r\n) are moved to LF(\n).
 *  User can use the csvimport function with different separator character.
 */
int csvimport(const char *filename, const char sep, csvimport_record_func recordcb, void *userdata) {
    FILE *fcsv;
    size_t fieldsz;
    char *fieldstart;
    char *ptr, *chk;
    char buffer[CSVLINESZ + 1];
    char *eol;
    csvctx_t ctx;
    csvimport_ctx_t importctx;


    if (!recordcb || filename == NULL || (filename != NULL && *filename == '\0')) {
        perror("invalid parameters");
        return(-1);
    }
    if ((fcsv = fopen(filename, "r")) == NULL) {
        perror(filename);
        return(-2);
    }

    memset(&ctx, 0, sizeof(csvctx_t));
    memset(&importctx, 0, sizeof(csvimport_ctx_t));
    importctx.csvctx = &ctx;
    ctx.filename = filename;
    while (!feof(fcsv)) {
        if (!importctx.escaped) {
            ctx.line_offset = ftello(fcsv);
        }
        if (fgets(buffer, CSVLINESZ, fcsv) == NULL && !feof(fcsv)) {
            #if defined(VERBOSE)
            fprintf(stderr, "csvimport: could not read file '%s'\n", filename);
            #endif
            importctx.rowcount = -1;
            break;
        } else if (feof(fcsv)) {
            break;
        }
        if ((eol = strrchr(buffer, '\n')) != NULL) {
            *eol = '\0';
        }
        if ((eol = strrchr(buffer, '\r')) != NULL) {
            *eol = '\0';
        }
        fieldstart = ptr = buffer;
        while (*ptr != '\0') {
            if (importctx.escaped) {
                if (*ptr == sep) {
                    if (ptr != fieldstart) {
                        chk = ptr - 1;
                        while (chk > fieldstart && *chk == '"') {
                            chk--;
                        }
                        if ((((size_t)(ptr - chk)) % 2) == 0) {
                            fieldsz = (size_t)(ptr - fieldstart);
                            if (csvimport_endfield(&importctx, fieldstart, fieldsz) != 0) {
                                #if defined(VERBOSE)
                                fprintf(stderr, "csvimport: endfield failed #1\n");
                                #endif
                                importctx.rowcount = -1;
                                break;
                            }
                            fieldstart = ptr + 1;
                        }
                    }
                }
            } else if (ptr == fieldstart && *ptr == '"') {
                importctx.escaped = true;
            } else if (*ptr == sep) {
                fieldsz = (size_t)(ptr - fieldstart);
                if (csvimport_endfield(&importctx, fieldstart, fieldsz) != 0) {
                    #if defined(VERBOSE)
                    fprintf(stderr, "csvimport: endfield failed #2\n");
                    #endif
                    importctx.rowcount = -1;
                    break;
                }
                fieldstart = ptr + 1;
            }
            ptr++;
        }
        if (importctx.rowcount == -1) {
            break;
        }
        if (!importctx.escaped) {
            fieldsz = (size_t)(ptr - fieldstart);
            if (csvimport_endfield(&importctx, fieldstart, fieldsz) != 0) {
                #if defined(VERBOSE)
                fprintf(stderr, "csvimport: endfield failed #3\n");
                #endif
                importctx.rowcount = -1;
                break;
            }
            if (*ptr == '\0') {
                if (csvimport_endrecord(&importctx, recordcb, userdata) != 0) {
                    #if defined(VERBOSE)
                    fprintf(stderr, "csvimport: endrecord failed #1\n");
                    #endif
                    importctx.rowcount = -1;
                    break;
                }
                continue;
            }
        } else {
            if (ptr != fieldstart) {
                chk = ptr - 1;
                while (chk > fieldstart && *chk == '"') {
                    chk--;
                }
                if ((((size_t)(ptr - chk)) % 2) == 0) {
                    fieldsz = (size_t)(ptr - fieldstart);
                    if (csvimport_endfield(&importctx, fieldstart, fieldsz) != 0) {
                        #if defined(VERBOSE)
                        fprintf(stderr, "csvimport: endfield failed #4\n");
                        #endif
                        importctx.rowcount = -1;
                        break;
                    }
                    if (*ptr == '\0') {
                        if (csvimport_endrecord(&importctx, recordcb, userdata) != 0) {
                            #if defined(VERBOSE)
                            fprintf(stderr, "csvimport: endrecord failed #2\n");
                            #endif
                            importctx.rowcount = -1;
                            break;
                        }
                        continue;
                    }
                }
            }
            fieldsz = (size_t)(ptr - fieldstart);
            if (csvimport_csvcontinues(&importctx, fieldstart, fieldsz) != 0) {
                #if defined(VERBOSE)
                fprintf(stderr, "csvimport: csvcontinues failed\n");
                #endif
                importctx.rowcount = -1;
            }
        }
    }

    if (fcsv != NULL) {
        fclose(fcsv);
    }
    csvimport_ctx_free(&importctx);

    return(importctx.rowcount);
}


int csvimport_buffer(const char *buffer, const char sep, csvimport_record_func recordcb, void *userdata) {
    size_t fieldsz;
    char *fieldstart;
    char *ptr, *chk;
    csvctx_t ctx;
    csvimport_ctx_t importctx;


    if (!recordcb || buffer == NULL || (buffer != NULL && *buffer == '\0')) {
        perror("invalid parameters");
        return(-1);
    }

    memset(&ctx, 0, sizeof(csvctx_t));
    memset(&importctx, 0, sizeof(csvimport_ctx_t));
    importctx.csvctx = &ctx;
    fieldstart = ptr = (char *)buffer;
    while (*ptr != '\0') {
        if (importctx.escaped) {
            if (*ptr == sep) {
                if (ptr != fieldstart) {
                    chk = ptr - 1;
                    while (chk > fieldstart && *chk == '"') {
                        chk--;
                    }
                    if ((((size_t)(ptr - chk)) % 2) == 0) {
                        fieldsz = (size_t)(ptr - fieldstart);
                        if (csvimport_endfield(&importctx, fieldstart, fieldsz) != 0) {
                            #if defined(VERBOSE)
                            fprintf(stderr, "csvimport_buffer: endfield failed #1\n");
                            #endif
                            importctx.rowcount = -1;
                            break;
                        }
                        fieldstart = ptr + 1;
                    }
                }
            }
        } else if (ptr == fieldstart && *ptr == '"') {
            importctx.escaped = true;
        } else if (*ptr == sep) {
            fieldsz = (size_t)(ptr - fieldstart);
            if (csvimport_endfield(&importctx, fieldstart, fieldsz) != 0) {
                #if defined(VERBOSE)
                fprintf(stderr, "csvimport_buffer: endfield failed #2\n");
                #endif
                importctx.rowcount = -1;
                break;
            }
            fieldstart = ptr + 1;
        }
        ptr++;
    }
    if (importctx.rowcount != -1) {
        if (!importctx.escaped) {
            fieldsz = (size_t)(ptr - fieldstart);
            if (csvimport_endfield(&importctx, fieldstart, fieldsz) != 0) {
                #if defined(VERBOSE)
                fprintf(stderr, "csvimport_buffer: endfield failed #3\n");
                #endif
                importctx.rowcount = -1;
            }
            if (*ptr == '\0') {
                if (csvimport_endrecord(&importctx, recordcb, userdata) != 0) {
                    #if defined(VERBOSE)
                    fprintf(stderr, "csvimport_buffer: endrecord failed #1\n");
                    #endif
                    importctx.rowcount = -1;
                }
            }
        } else {
            if (ptr != fieldstart) {
                chk = ptr - 1;
                while (chk > fieldstart && *chk == '"') {
                    chk--;
                }
                if ((((size_t)(ptr - chk)) % 2) == 0) {
                    fieldsz = (size_t)(ptr - fieldstart);
                    if (csvimport_endfield(&importctx, fieldstart, fieldsz) != 0) {
                        #if defined(VERBOSE)
                        fprintf(stderr, "csvimport_buffer: endfield failed #4\n");
                        #endif
                        importctx.rowcount = -1;
                    }
                    if (*ptr == '\0') {
                        if (csvimport_endrecord(&importctx, recordcb, userdata) != 0) {
                            #if defined(VERBOSE)
                            fprintf(stderr, "csvimport_buffer: endrecord failed #2\n");
                            #endif
                            importctx.rowcount = -1;
                        }
                    }
                }
            }
        }
    }
    csvimport_ctx_free(&importctx);

    return(importctx.rowcount < 0 ? -1 : 0);
}


int linesimport(const char *filename, const char sep, linesimport_record_func recordcb, void *userdata) {
    FILE *fcsv;
    size_t sz, prevbufsz;
    char *fieldstart;
    char *ptr, *chk;
    char buffer[CSVLINESZ + 1];
    char *eol;
    linectx_t ctx;
    csvimport_ctx_t importctx;
    int res;


    if (!recordcb || filename == NULL || (filename != NULL && *filename == '\0')) {
        perror("invalid parameters");
        return(-1);
    }
    if ((fcsv = fopen(filename, "r")) == NULL) {
        perror(filename);
        return(-2);
    }

    memset(&ctx, 0, sizeof(csvctx_t));
    memset(&importctx, 0, sizeof(csvimport_ctx_t));
    ctx.filename = filename;
    while (!feof(fcsv)) {
        if (!importctx.escaped) {
            ctx.line_offset = ftello(fcsv);
        }
        if (fgets(buffer, CSVLINESZ, fcsv) == NULL && !feof(fcsv)) {
            #if defined(VERBOSE)
            fprintf(stderr, "linesimport: could not read file '%s'\n", filename);
            #endif
            importctx.rowcount = -1;
            break;
        } else if (feof(fcsv)) {
            break;
        }
        if ((eol = strrchr(buffer, '\n')) != NULL) {
            *eol = '\0';
        }
        if ((eol = strrchr(buffer, '\r')) != NULL) {
            *eol = '\0';
        }
        fieldstart = ptr = buffer;
        while (*ptr != '\0') {
            if (importctx.escaped) {
                if (*ptr == sep) {
                    if (ptr != fieldstart) {
                        chk = ptr - 1;
                        while (chk > fieldstart && *chk == '"') {
                            chk--;
                        }
                        if ((((size_t)(ptr - chk)) % 2) == 0) {
                            importctx.escaped = false;
                            ctx.fieldscount++;
                            fieldstart = ptr + 1;
                        }
                    }
                }
            } else if (ptr == fieldstart && *ptr == '"') {
                importctx.escaped = true;
            } else if (*ptr == sep) {
                ctx.fieldscount++;
                fieldstart = ptr + 1;
            }
            ptr++;
        }
        if (importctx.rowcount == -1) {
            break;
        }
        if (!importctx.escaped) {
            ctx.fieldscount++;
            if (*ptr == '\0') {
                if (importctx.prevbuf == NULL) {
                    ctx.csvline = strdup(buffer);
                    if (ctx.csvline == NULL) {
                        importctx.rowcount = -1;
                        break;
                    }
                } else {
                    sz = strlen(importctx.prevbuf) + 1 + strlen(buffer) + 1;
                    ctx.csvline = calloc(sz, sizeof(char));
                    if (ctx.csvline == NULL) {
                        free(importctx.prevbuf);
                        importctx.rowcount = -1;
                        break;
                    }
                    strcpy(ctx.csvline, importctx.prevbuf);
                    strcat(ctx.csvline, "\n");
                    strcat(ctx.csvline, buffer);
                    free(importctx.prevbuf);
                    importctx.prevbuf = NULL;
                }
                if ((res = recordcb(&ctx, userdata)) == 0) {
                    free(ctx.csvline);
                    ctx.csvline = NULL;
                    ctx.fieldscount = 0;
                } else if (res > 0) {
                    ctx.csvline = NULL;
                    ctx.fieldscount = 0;
                } else {
                    free(ctx.csvline);
                    importctx.rowcount = -1;
                    break;
                }
                importctx.rowcount++;
                continue;
            }
        } else {
            if (ptr != fieldstart) {
                chk = ptr - 1;
                while (chk > fieldstart && *chk == '"') {
                    chk--;
                }
                if ((((size_t)(ptr - chk)) % 2) == 0) {
                    importctx.escaped = false;
                    ctx.fieldscount++;
                    if (*ptr == '\0') {
                        if (importctx.prevbuf != NULL) {
                            sz = strlen(importctx.prevbuf) + 1 + strlen(buffer) + 1;
                            ctx.csvline = calloc(sz, sizeof(char));
                            if (ctx.csvline == NULL) {
                                free(importctx.prevbuf);
                                importctx.rowcount = -1;
                                break;
                            }
                            strcpy(ctx.csvline, importctx.prevbuf);
                            strcat(ctx.csvline, "\n");
                            strcat(ctx.csvline, buffer);
                            free(importctx.prevbuf);
                            importctx.prevbuf = NULL;
                        } else {
                            ctx.csvline = strdup(buffer);
                            if (ctx.csvline == NULL) {
                                importctx.rowcount = -1;
                                break;
                            }
                        }
                        if ((res = recordcb(&ctx, userdata)) == 0) {
                            free(ctx.csvline);
                            ctx.csvline = NULL;
                            ctx.fieldscount = 0;
                        } else if (res > 0) {
                            ctx.csvline = NULL;
                            ctx.fieldscount = 0;
                        } else {
                            free(ctx.csvline);
                            importctx.rowcount = -1;
                            break;
                        }
                        importctx.rowcount++;
                        continue;
                    }
                }
            }
            if (importctx.prevbuf == NULL) {
                importctx.prevbuf = strdup(buffer);
                if (importctx.prevbuf == NULL) {
                    importctx.rowcount = -1;
                    break;
                }
            } else {
                prevbufsz = strlen(importctx.prevbuf);
                sz = prevbufsz + 1 + strlen(buffer) + 1;
                importctx.prevbuf = realloc(importctx.prevbuf, sizeof(char) * sz);
                if (importctx.prevbuf == NULL) {
                    importctx.rowcount = -1;
                    break;
                }
                importctx.prevbuf[prevbufsz] = '\0';
                strcat(importctx.prevbuf, "\n");
                strcat(importctx.prevbuf, buffer);
            }
        }
    }

    if (fcsv != NULL) {
        fclose(fcsv);
    }
    csvimport_ctx_free(&importctx);

    return(importctx.rowcount);
}


/*
 *
 */
static void csvimport_ctx_free(csvimport_ctx_t *importctx) {
    size_t index;

    if (importctx != NULL) {
        if (importctx->prevbuf != NULL) {
            free(importctx->prevbuf);
        }
        if (importctx->fields != NULL) {
            for (index = 0; index < importctx->fieldscount; index++) {
                free(importctx->fields[index]);
            }
            free(importctx->fields);
            free(importctx->fieldssz);
        }
    }
}


/*
 *
 */
static int csvimport_managefield(csvimport_ctx_t *importctx, size_t fieldsz, char **curfield, size_t *cursz) {
    char **ofields;
    size_t *ofieldssz;
    char *oprevbuf;

    if ((importctx->csvctx->fieldscount + 1) > importctx->fieldscount) {
        ofields = importctx->fields;
        importctx->fields = realloc(ofields, sizeof(char *) * (importctx->fieldscount + 1));
        if (importctx->fields == NULL) {
            #if defined(VERBOSE)
            fprintf(stderr, "csvimport_managefield: could not allocate import fields\n");
            #endif
            importctx->fields = ofields;
            csvimport_ctx_free(importctx);
            return(-1);
        }
        ofieldssz = importctx->fieldssz;
        importctx->fieldssz = realloc(ofieldssz, sizeof(size_t) * (importctx->fieldscount + 1));
        if (importctx->fieldssz == NULL) {
            #if defined(VERBOSE)
            fprintf(stderr, "csvimport_managefield: could not allocate import fields size\n");
            #endif
            importctx->fieldssz = ofieldssz;
            csvimport_ctx_free(importctx);
            return(-2);
        }
        importctx->fields[importctx->fieldscount] = NULL;
        importctx->fieldssz[importctx->fieldscount] = 0;
        importctx->fieldscount++;
    }

    if (importctx->prevbuf == NULL) {
        if ((fieldsz > importctx->fieldssz[importctx->csvctx->fieldscount])
            || (fieldsz == 0 && importctx->fieldssz[importctx->csvctx->fieldscount] == 0)) {
            if (importctx->fields[importctx->csvctx->fieldscount] != NULL) {
                free(importctx->fields[importctx->csvctx->fieldscount]);
            }
            importctx->fields[importctx->csvctx->fieldscount] = calloc(fieldsz + 1, sizeof(char));
            if (importctx->fields[importctx->csvctx->fieldscount] == NULL) {
                #if defined(VERBOSE)
                fprintf(stderr, "csvimport_managefield: could not allocate current field\n");
                #endif
                csvimport_ctx_free(importctx);
                return(-3);
            }
            importctx->fieldssz[importctx->csvctx->fieldscount] = fieldsz;
        } else if (importctx->fieldssz[importctx->csvctx->fieldscount] > 0) {
            memset(importctx->fields[importctx->csvctx->fieldscount], 0, importctx->fieldssz[importctx->csvctx->fieldscount]);
        }
    } else {
        oprevbuf = importctx->prevbuf;
        importctx->prevbuf = realloc(oprevbuf, sizeof(char) * (importctx->prevbufsz + fieldsz + 1));
        if (importctx->prevbuf == NULL) {
            #if defined(VERBOSE)
            fprintf(stderr, "csvimport_managefield: could not allocate prevbuf\n");
            #endif
            importctx->prevbuf = oprevbuf;
            csvimport_ctx_free(importctx);
            return(-4);
        }
        memset(importctx->prevbuf + importctx->prevbufsz, 0, fieldsz + 1);
        importctx->prevbufsz += fieldsz;
        if (importctx->fields[importctx->csvctx->fieldscount] != NULL) {
            free(importctx->fields[importctx->csvctx->fieldscount]);
        }
        importctx->fields[importctx->csvctx->fieldscount] = importctx->prevbuf;
        importctx->fieldssz[importctx->csvctx->fieldscount] = importctx->prevbufsz;
        importctx->prevbuf = NULL;
        importctx->prevbufsz = 0;

    }
    *curfield = importctx->fields[importctx->csvctx->fieldscount];
    *cursz = importctx->fieldssz[importctx->csvctx->fieldscount];
    #if defined(VERBOSE)
    fprintf(stderr, "csvimport_managefield: fieldsz=%zu, cursz=%zu\n", fieldsz, *cursz);
    #endif

    return(0);
}


/*
 *
 */
static int csvimport_endfield(csvimport_ctx_t *importctx, char *fieldstart, size_t fieldsz) {
    bool prevbuf;
    size_t prevbufsz;
    char *curfield;
    size_t cursz;

    #if defined(VERBOSE)
    fprintf(stderr, "csvimport_endfield: fieldstart=[%s], fieldsz=%zu\n", fieldstart, fieldsz);
    #endif
    curfield = NULL;
    cursz = 0;
    prevbuf = importctx->prevbuf != NULL;
    prevbufsz = importctx->prevbufsz;
    if (csvimport_managefield(importctx, fieldsz, &curfield, &cursz) != 0) {
        return(-1);
    }
    #if defined(VERBOSE)
    fprintf(stderr, "csvimport_endfield: cursz=%zu\n", cursz);
    #endif
    if (fieldsz > 0) {
        if (!prevbuf) {
            memcpy(curfield, fieldstart, fieldsz);
        } else {
            memcpy(curfield + prevbufsz, fieldstart, fieldsz);
            prevbufsz += fieldsz;
            curfield[prevbufsz] = '\0';
        }
        #if defined(VERBOSE)
        fprintf(stderr, "csvimport_endfield: curfield=[%s], strlen=%zu\n", curfield, strlen(curfield));
        #endif
    }
    if ((fieldsz > 0 || (cursz > 0 && prevbuf)) && importctx->escaped) {
        csvimport_unescape(curfield);
        importctx->escaped = false;
    }
    #if defined(VERBOSE)
    fprintf(stderr, "csvimport_endfield: end: curfield=[%s], strlen=%zu\n", curfield, strlen(curfield));
    #endif

    importctx->csvctx->fieldscount++;

    return(0);
}

/*
 *
 */
static int csvimport_endrecord(csvimport_ctx_t *importctx, csvimport_record_func recordcb, void *userdata) {
    int res;
    size_t index, sz;
    char *data, *pdata;

    #if defined(VERBOSE)
    fprintf(stderr, "csvimport_endrecord: rowindex=%zu\n", importctx->rowcount);
    fprintf(stderr, "csvimport_endrecord: importctx->csvctx->fieldscount=%zu, importctx->fieldscount=%zu\n", importctx->csvctx->fieldscount, importctx->fieldscount);
    #endif

    sz = 0;
    for (index = 0; index < importctx->csvctx->fieldscount; index++) {
        sz += (strlen(importctx->fields[index]) + 1);
        #if defined(VERBOSE)
        fprintf(stderr, "csvimport_endrecord: sz=%zu\n", sz);
        #endif
    }
    data = calloc(sz, sizeof(char));
    if (data == NULL) {
        #if defined(VERBOSE)
        fprintf(stderr, "csvimport_endrecord: could not allocate data buffer\n");
        #endif
        csvimport_ctx_free(importctx);
        return(-1);        
    }
    importctx->csvctx->fields = calloc(importctx->csvctx->fieldscount, sizeof(char *));
    if (importctx->csvctx->fields == NULL) {
        #if defined(VERBOSE)
        fprintf(stderr, "csvimport_endrecord: could not allocate fields\n");
        #endif
        free(data);
        csvimport_ctx_free(importctx);
        return(-1);        
    }
    pdata = data;
    for (index = 0; index < importctx->csvctx->fieldscount; index++) {
        if (*(importctx->fields[index]) != '\0') {
            strcat(pdata, importctx->fields[index]);
        }
        importctx->csvctx->fields[index] = pdata;
        pdata += (strlen(importctx->fields[index]) + 1);
    }
    if ((res = recordcb(importctx->csvctx, userdata)) == 0) {
        free(data);
        free(importctx->csvctx->fields);
        importctx->csvctx->fields = NULL;
        importctx->csvctx->fieldscount = 0;
        importctx->escaped = false;
    } else if (res < 0) {
        #if defined(VERBOSE)
        fprintf(stderr, "csvimport_endrecord: recordcd returned a negative value\n");
        #endif
        free(data);
        free(importctx->csvctx->fields);
        csvimport_ctx_free(importctx);
        return(-1);
    } else if (res > 0) {
        importctx->csvctx->fields = NULL;
        importctx->csvctx->fieldscount = 0;
        importctx->escaped = false;
    }
    importctx->rowcount++;

    return(0);
}

/*
 *
 */
static int csvimport_csvcontinues(csvimport_ctx_t *importctx, char *fieldstart, size_t fieldsz) {
    char *oprevbuf;

    oprevbuf = importctx->prevbuf;
    importctx->prevbuf = realloc(oprevbuf, sizeof(char) * (importctx->prevbufsz + (fieldsz + 1) + 1));
    if (importctx->prevbuf == NULL) {
        #if defined(VERBOSE)
        fprintf(stderr, "csvimport_csvcontinues: could not allocate prevbuf\n");
        #endif
        importctx->prevbuf = oprevbuf;
        csvimport_ctx_free(importctx);
        return(-1);
    } else {
        memcpy(importctx->prevbuf + importctx->prevbufsz, fieldstart, fieldsz);
        importctx->prevbuf[importctx->prevbufsz + fieldsz] = '\n';
        importctx->prevbufsz += (fieldsz + 1);
        importctx->prevbuf[importctx->prevbufsz] = '\0';
    }

    return(0);
}


/*
 * desc: unescape a field value that's been escaped (RFC 4180)
 */
static size_t csvimport_unescape(char *dst) {
    char *ptr, *end;
    size_t len;

    if (dst == NULL || (dst != NULL && *dst == '\0')) {
        return(0);
    }
    len = strlen(dst);
    end = dst + len;
    ptr = dst;
    while (ptr < end) {
        if (*ptr == '\"' && *(ptr + 1) == '\"') {
            memcpy(ptr, ptr + 1, end - ptr);
            ptr++;
            end--; 
        }
        ptr++;
    }
    len = (size_t)(end - dst) - 1;
    dst[len] = '\0';
    memmove(dst, dst + 1, len);
    return(len - 1);
}
