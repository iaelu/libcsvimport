#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "csvimport.h"

static int test_record(csvctx_t *ctx, void *userdata);
static int test_line_record(linectx_t *ctx, void *userdata);


int main(void) {
    int records;
    char *buffer;

    if ((records = csvimport("test.csv", ';', test_record, NULL)) <= -1) {
        perror("csvimport");
        return(2);
    }

    printf("#records=%d\n", records);
    printf("\n\n===================================================================\n\n");

    if ((records = linesimport("test.csv", ';', test_line_record, NULL)) <= -1) {
        perror("linesimport");
        return(3);
    }

    printf("#records=%d\n", records);
    printf("\n\n===================================================================\n\n");

    buffer = strdup("value1;value2;\"value3\"\";\n\n;\"\" \"\" value3 \"\" \"");
    if (csvimport_buffer(buffer, ';', test_record, NULL) != 0) {
        perror("csvimport_buffer");
        return(4);
    }

    return(0);
}


static int test_record(csvctx_t *ctx, void *userdata) {
    size_t index;

    printf("\n=========== off=%ld\n", ctx->line_offset);

    printf("%zu\n", ctx->fieldscount);
    for (index = 0; index < ctx->fieldscount; index++) {
        printf("[%s]", ctx->fields[index]);
    }

    printf("\n===========\n");

    return(0);
}


static int test_line_record(linectx_t *ctx, void *userdata) {

    printf("\n=========== off=%ld\n", ctx->line_offset);

    printf("fields# [%zu]\n", ctx->fieldscount);
    printf("[%s]", ctx->csvline);

    printf("\n===========\n");

    return(0);
}
